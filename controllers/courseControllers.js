// Import Course Model
const Course = require("../models/Course");

// Add Course
module.exports.addCourse = (req, res) => {

	console.log(req.body);

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})
	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err))
};

//Retrieve All Courses

module.exports.getAllCourses = (req, res) => {

	Course.find()
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// ACTIVITY 3

//Retrieve a Single Course

module.exports.getSingleCourse = (req, res) => {
	console.log(req.params);

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// ACTIVITY 4

// archiving a course

module.exports.archiveCourse = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: false})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

};


// // activate a course

module.exports.activateCourse = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

};

// get active courses

module.exports.getActiveCourses = (req, res) => {

	Course.find({isActive: true})
	.then(activeCourses => res.send(activeCourses))
	.catch(err => res.send(err));

};

// updating course

module.exports.updateCourse = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};

// get inactive courses

module.exports.getInactiveCourses = (req, res) => {

	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// find courses by name

module.exports.findCoursesByName = (req, res) => {

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send("No courses found");
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

// find courses by price

module.exports.findCoursesByPrice = (req, res) => {

	Course.find({price: req.body.price})
	.then(result => {

		console.log(result)
		if(result.length === 0){
			return res.send("No course found");
		
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};

module.exports.getEnrollees = (req, res) => {
		console.log(req.params.id);
	Course.findById(req.params.id)
		.then(course => res.send(course.enrollees))
		.catch(error => res.send(error))
}





















