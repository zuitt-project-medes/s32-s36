const express = require('express');
const router = express.Router();

// import user controllers
const userControllers = require("../controllers/userControllers");


const auth = require("../auth");

const {verify, verifyAdmin} = auth;

// Routes

// User Registration
router.post("/", userControllers.registerUser);

// Retrieve All Users
router.get("/", userControllers.getAllUsers);

// Login
router.post("/login", userControllers.loginUser);


// Retrieve User Details
router.get("/getUserDetails", verify, userControllers.getUserDetails)

// Stretch Goals
//Checking email exists
router.post("/checkEmailExists", userControllers.checkEmailExists);


// Mini Activity

// update regular User to Admin
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

// update user details
router.put("/updateUserDetails/:id", verify, userControllers.updateUserDetails);

// // Enroll registered user

router.post("/enroll", verify, userControllers.enroll);

// get enrollments
router.get("/getEnrollments", verify, userControllers.getEnrollments);

module.exports = router;











